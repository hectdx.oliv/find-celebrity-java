package test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindCelebrityTest {

    private CelebrityFinder finder = new CelebrityFinder();

    @Test
    public void testFindCelebrityShouldPassWhenCelebrityExists(){
        List<Person> lstPeopleWithCelebrity = createGroupWithCelebrity();

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithCelebrity);

        Assertions.assertNotNull(celebrityList);
        Assertions.assertFalse(celebrityList.isEmpty());
        Assertions.assertTrue(celebrityList.get(0).getKnownPersons().isEmpty());
        Assertions.assertEquals("celeb", celebrityList.get(0).getName());
    }


    @Test
    public void testFindCelebrityShouldPassWhenCelebrityDoesNotExist(){
        List<Person> lstPeopleWithoutCelebrity = createGroupWithoutCelebrityWhenPersonKnowsSomeone();

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithoutCelebrity);

        Assertions.assertTrue(celebrityList.isEmpty());
    }

    @Test
    public void testFindCelebrityShouldPassWhenCelebrityDoesNotExistBut(){
        List<Person> lstPeopleWithoutCelebrity = createGroupWithoutCelebrityButAPopularPerson();

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithoutCelebrity);

        Assertions.assertTrue(celebrityList.isEmpty());
    }

    @Test
    public void testFindCelebrityShouldPassWhenOnePerson(){
        List<Person> lstPeopleWithoutCelebrity = Arrays.asList(new Person("p1"));

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithoutCelebrity);

        Assertions.assertTrue(celebrityList.isEmpty());
    }

    @Test
    public void testFindCelebrityShouldPassWhenEmptyList(){
        List<Person> lstPeopleWithoutCelebrity = null;

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithoutCelebrity);

        Assertions.assertTrue(celebrityList.isEmpty());
    }

    @Test
    public void testFindCelebrityShouldPassWhenMoreThanOneCelebrity(){
        List<Person> lstPeopleWithCelebrities = createGroupWithoutTwoCelebrities();

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithCelebrities);

        Assertions.assertFalse(celebrityList.isEmpty());
        Assertions.assertTrue(celebrityList.get(0).getKnownPersons().isEmpty());
        Assertions.assertEquals("celeb1", celebrityList.get(0).getName());
        Assertions.assertTrue(celebrityList.get(1).getKnownPersons().isEmpty());
        Assertions.assertEquals("celeb2", celebrityList.get(1).getName());
    }

    @Test
    public void testFindCelebrityShouldPassWhenAlonePeople(){
        List<Person> lstPeopleWithCelebrities = createGroupNoOneKnows();

        List<Person> celebrityList = finder.findCelebrity(lstPeopleWithCelebrities);

        Assertions.assertTrue(celebrityList.isEmpty());
    }


    /**
     * Celebrity is known by all and doesn't know anybody
     * @return
     */
    private static List<Person> createGroupWithCelebrity(){
        List<Person> people = new ArrayList<Person>();

        Person p1 = new Person();
        p1.setName("p1");
        p1.getKnownPersons().add(new Person("p2"));
        p1.getKnownPersons().add(new Person("celeb"));
        people.add(p1);

        Person p2 = new Person();
        p2.setName("p2");
        p2.getKnownPersons().add(new Person("p1"));
        p2.getKnownPersons().add(new Person("celeb"));
        people.add(p2);

        Person p3 = new Person();
        p3.setName("p3");
        p3.getKnownPersons().add(new Person("p1"));
        p3.getKnownPersons().add(new Person("celeb"));
        people.add(p3);

        Person celeb = new Person();
        celeb.setName("celeb");
        //doesn't know anybody
        people.add(celeb);

        return people;
    }

    /**
     * All people knows someone
     * @return
     */
    private static List<Person> createGroupWithoutCelebrityWhenPersonKnowsSomeone(){
        List<Person> people = new ArrayList<Person>();

        Person p1 = new Person();
        p1.setName("p1");
        p1.getKnownPersons().add(new Person("p2"));
        p1.getKnownPersons().add(new Person("p3"));
        people.add(p1);

        Person p2 = new Person();
        p2.setName("p2");
        p2.getKnownPersons().add(new Person("p1"));
        p2.getKnownPersons().add(new Person("p3"));
        people.add(p2);

        Person p3 = new Person();
        p3.setName("p3");
        p3.getKnownPersons().add(new Person("p2"));
        people.add(p3);

        return people;
    }

    /**
     * A  person is known by everybody and he/she knows people
     * @return
     */
    private static List<Person> createGroupWithoutCelebrityButAPopularPerson(){
        List<Person> people = new ArrayList<Person>();

        Person p1 = new Person();
        p1.setName("popular");
        p1.getKnownPersons().add(new Person("p2"));
        p1.getKnownPersons().add(new Person("p3"));
        people.add(p1);

        Person p2 = new Person();
        p2.setName("p2");
        p2.getKnownPersons().add(new Person("popular"));
        p2.getKnownPersons().add(new Person("p3"));
        people.add(p2);

        Person p3 = new Person();
        p3.setName("p3");
        p3.getKnownPersons().add(new Person("popular"));
        people.add(p3);



        return people;
    }

    /**
     * Weird case when two celebrities doesn't know each other
    * */
    private static List<Person> createGroupWithoutTwoCelebrities(){
        List<Person> people = new ArrayList<Person>();

        Person p1 = new Person();
        p1.setName("celeb1");
        people.add(p1);

        Person p2 = new Person();
        p2.setName("celeb2");
        people.add(p2);

        Person p3 = new Person();
        p3.setName("p3");
        p3.getKnownPersons().add(p1);
        p3.getKnownPersons().add(p2);
        p3.getKnownPersons().add(new Person("p4"));
        people.add(p3);

        Person p4 = new Person();
        p4.setName("p4");
        p4.getKnownPersons().add(p1);
        p4.getKnownPersons().add(p2);
        people.add(p4);


        return people;
    }

    /**
     * People that doesn't know anybody
     * @return
     */
    private List<Person> createGroupNoOneKnows(){
        List<Person> people = new ArrayList<Person>();

        Person p1 = new Person();
        p1.setName("alone 1");
        people.add(p1);

        Person p2 = new Person();
        p2.setName("alone 2");
        people.add(p2);

        return people;
    }
}
