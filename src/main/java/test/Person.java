package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Person {

    private String name;
    private List<Person> knownPersons = new ArrayList<Person>();

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getKnownPersons() {
        return knownPersons;
    }

    public void setKnownPersons(List<Person> knownPersons) {
        this.knownPersons = knownPersons;
    }

    //equals takes into consideration the name only
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", knownPersons=" + knownPersons +
                '}';
    }
}
