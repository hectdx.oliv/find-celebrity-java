package test;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Exercise:
 * In a team of N people, a celebrity is known by everyone but he/she doesn't know anybody.
 */
public class CelebrityFinder {

    private final static Logger logger = LoggerFactory.getLogger(CelebrityFinder.class);

    /**
     * Find a celebrity from a group of persons
     *
     * @param people
     * @return The celebrity or NULL if no exists
     */
    public List<Person> findCelebrity(final List<Person> people){
        List<Person> celebritiesFoundList = new ArrayList<>();
        logger.info("People: {}", people);
        // default cases
        if (people == null || people.isEmpty() || people.size() == 1){
            logger.warn("Default case. No Celebrity");
            return celebritiesFoundList;
        }

        //a celebrity doesn't know anybody
        List<Person> possibleCelebritiesList = people.stream()
                .filter(p -> p.getKnownPersons().isEmpty()).collect(Collectors.toList());

        logger.info("Possible celebrities found: {}", possibleCelebritiesList.size());

        List<Person> normalPersonList = null;
        for (Person celeb : possibleCelebritiesList){
            logger.info("Checking possible celebrity... {}", celeb.getName());

            normalPersonList = people.stream()
                    .filter(p -> !possibleCelebritiesList.contains(p)) //ignore the celebrity person
                    .collect(Collectors.toList());

            if (normalPersonList.isEmpty()) break; //nothing to evaluate

            boolean knownByAll = normalPersonList.stream()
                    .allMatch( p -> {
                        logger.debug("Check person: {}", p);
                        boolean knowsCelebrity = p.getKnownPersons().contains(celeb);
                        logger.debug("Knows celebrity {}? {}", celeb.getName(), knowsCelebrity);
                        return knowsCelebrity;
                    });

            if (knownByAll){
                celebritiesFoundList.add(celeb);
            }
        }

        return celebritiesFoundList;
    }
}
